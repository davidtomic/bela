package qa.david.bela;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.deck.CardColor;
import qa.david.bela.deck.CardType;
import qa.david.bela.deck.Deck;

/**
 * Created by dado on 16.04.16..
 */

public class TableDeck {

    private List<Card> deck;

    TableDeck(List<Card> deck) {
        this.deck = deck;
    }

    public List<Card> getDeck() {
        return deck;
    }
}
