package qa.david.bela;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.team.Team;

/**
 * Created by Pc on 27.7.2017..
 */

public class TableTeam {

    private Team team;

    public TableTeam(Team team) {
        this.team = team;
    }

    public Team getTeam() {
        return team;
    }
}
