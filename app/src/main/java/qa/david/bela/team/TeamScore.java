package qa.david.bela.team;

/**
 * Created by Pc on 4.8.2017..
 */

public class TeamScore {

    private int totalTeamScore;
    private int teamScore;

    public int getTotalTeamScore() {
        return totalTeamScore;
    }

    public void setTotalTeamScore(int totalTeamScore) {
        this.totalTeamScore = totalTeamScore;
    }

    public int getTeamScoreMi() {
        return teamScore;
    }

    public void setTeamScoreMi(int teamScore) {
        this.teamScore = teamScore;
    }


}
