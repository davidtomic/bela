package qa.david.bela.team;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.player.Player;

/**
 * Created by Pc on 27.7.2017..
 */

public class Team {

    private String name;
    private List<Player> teamPlayers = new ArrayList<>(2);
    private List<Card> wonCards = new ArrayList<>(32);
    private TeamScore score;

    public Team(String name, List<Player> teamPlayers) {
        this.name = name;
        this.teamPlayers = teamPlayers;
    }

    public void setWonCards(List<Card> wonCards) {
        this.wonCards.addAll(wonCards);
    }

    public List<Card> getTeams() {
        return wonCards;
    }

    public List<Player> getTeamPlayers() {
        return teamPlayers;
    }

    public String getName() {
        return name;
    }
}
