package qa.david.bela.extra.classes;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import qa.david.bela.deck.Card;

public class CardImageView extends android.support.v7.widget.AppCompatImageView {

    private Card card;

    public CardImageView(Context context) {
        super(context);
    }

    public CardImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCard(Card card){
        this.card = card;
        setImageResource(card.getImage());
    }

    public Card getCard() {
        return card;
    }

}
