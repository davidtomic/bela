package qa.david.bela.extra.classes;

import android.content.Context;
import android.util.AttributeSet;

import qa.david.bela.deck.Card;

/**
 * Created by Pc on 26.7.2017..
 */

public class PlayerButton extends android.support.v7.widget.AppCompatImageButton {

    private Card card;

    public PlayerButton(Context context) {
        super(context);
    }

    public PlayerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlayerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setCard(Card card) {
        if (card == null) {
            setBackground(null);
        } else {
            setBackground(getResources().getDrawable(card.getImage()));
        }
        this.card = card;
    }

    public Card getCard(){
        return card;
    }

}