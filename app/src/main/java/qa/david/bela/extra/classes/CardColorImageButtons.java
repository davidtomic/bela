package qa.david.bela.extra.classes;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

import qa.david.bela.deck.Card;
import qa.david.bela.deck.CardColor;

public class CardColorImageButtons extends android.support.v7.widget.AppCompatImageButton {

    private Card card;

    public CardColorImageButtons(Context context) {
        super(context);
    }

    public CardColorImageButtons(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardColorImageButtons(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCard(Card card){
        this.card = card;
        setImageResource(card.getCardColor().getImage());
    }

    public Card getCard() {
        return card;
    }
}
