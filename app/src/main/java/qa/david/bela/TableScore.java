package qa.david.bela;

/**
 * Created by Pc on 26.7.2017..
 */

public class TableScore {

    private int miScoreUkupno;
    private int viScoreUkupno;
    private int miScoreTrenutno;
    private int getViScoreUkupno;
    private int miZvanja;
    private int viZvanja;

    public int getMiZvanja() {
        return miZvanja;
    }

    public void setMiZvanja(int miZvanja) {
        this.miZvanja = miZvanja;
    }

    public int getViZvanja() {
        return viZvanja;
    }

    public void setViZvanja(int viZvanja) {
        this.viZvanja = viZvanja;
    }

    public void setMiScoreUkupno(int miScoreUkupno) {
        this.miScoreUkupno = miScoreUkupno;
    }

    public void setViScoreUkupno(int viScoreUkupno) {
        this.viScoreUkupno = viScoreUkupno;
    }

    public void setMiScoreTrenutno(int miScoreTrenutno) {
        this.miScoreTrenutno = miScoreTrenutno;
    }

    public void setGetViScoreUkupno(int getViScoreUkupno) {
        this.getViScoreUkupno = getViScoreUkupno;
    }

    public int getMiScoreUkupno() {
        return miScoreUkupno;
    }

    public int getViScoreUkupno() {
        return viScoreUkupno;
    }

    public int getMiScoreTrenutno() {
        return miScoreTrenutno;
    }

    public int getGetViScoreUkupno() {
        return getViScoreUkupno;
    }
}
