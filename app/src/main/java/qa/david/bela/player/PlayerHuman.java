package qa.david.bela.player;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.deck.CardColor;

/**
 * Created by Pc on 26.7.2017..
 */

public class PlayerHuman implements Player {

    private final static int MAX_NUMBER_OF_PLAYER_CARDS = 8;

    private String name;
    private Card cardOnTable;
    private List<Card> playerCards= new ArrayList<>(MAX_NUMBER_OF_PLAYER_CARDS);
    private boolean onMove;

    public PlayerHuman(String name){
        this.name = name;
    }

    public void playedCard(Card card1, List<Card> cardsOnTable, CardColor cardAdut) {

        boolean haveAdut=false;
        boolean haveColor=false;
        boolean haveHigher=false;
        boolean haveHigherAdut=false;
        boolean adutOn=false;
        boolean colorOn=false;


        if(cardsOnTable.get(0)!=null) {
            for (Card card : getPlayerCards()) {
                if (card.getCardColor() == cardAdut) {
                    haveAdut = true;
                    break;
                }
                if (card.getCardColor() == cardsOnTable.get(0).getCardColor()) {
                    haveColor = true;
                    break;
                }
            }

            for (Card card : cardsOnTable) {
                if (card.getCardColor() == cardAdut) {
                    adutOn = true;
                    break;
                }
            }

            for (Card card : getPlayerCards()) {
                if (adutOn) {
                    if (haveAdut) {
                        if (card1.getValue() > cardsOnTable.get(0).getValue()) {
                            haveHigherAdut = true;
                        }
                    }
                } else {
                    if (haveColor) {
                        if (card1.getValue() > cardsOnTable.get(0).getValue()) {
                            haveHigher = true;
                        }
                    }
                }
            }

            if (adutOn) {
                if (haveAdut) {
                    if (haveHigherAdut) {
                        if (card1.getCardColor() == cardAdut) {
                            if (card1.getValue() > cardsOnTable.get(0).getValue()) {
                                cardOnTable = card1;
                            }
                        }
                    } else {
                        if (card1.getCardColor() == cardAdut) {
                            cardOnTable = card1;
                        }
                    }
                } else {
                    if (haveColor) {
                        if (card1.getCardColor() == cardsOnTable.get(0).getCardColor()) {
                            cardOnTable = card1;
                        }
                    }
                }
            } else {
                if (haveColor) {
                    if (haveHigher) {
                        if (card1.getCardColor() == cardOnTable.getCardColor()) {
                            if (card1.getValue() > cardOnTable.getValue()) {
                                cardOnTable = card1;
                            }
                        }
                    } else {
                        if (card1.getCardColor() == cardOnTable.getCardColor()) {
                            cardOnTable = card1;
                        }
                    }
                } else {
                    cardOnTable = card1;
                }
            }
        } else {
            cardOnTable=card1;
        }
    }

    public void givePlayerCard(Card card){
        playerCards.add(card);
    }

    @Override
    public Card getPlayedCard() {
        return cardOnTable;
    }

    public String getName() {
        return name;
    }

    public List<Card> getPlayerCards(){
        return playerCards;
    }

    public CardColor zovi(){
        return null;
    }

    @Override
    public void setOnMove(boolean onMove) {
        this.onMove = onMove;
    }

    @Override
    public boolean onMove() {
        return onMove;
    }

}
