package qa.david.bela.player;

import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.deck.CardColor;
import qa.david.bela.extra.classes.CardImageView;
import qa.david.bela.extra.classes.PlayerButton;
import qa.david.bela.TableCards;

/**
 * Created by Pc on 27.7.2017..
 */

public class PlayerDroid implements Player {

    private final static int MAX_NUMBER_OF_PLAYER_CARDS = 8;

    private String name;
    private Card cardOnTable;
    private ArrayList<Card> playerCards = new ArrayList<>(MAX_NUMBER_OF_PLAYER_CARDS);
    private boolean onMove;

    public PlayerDroid(String name){
        this.name = name;
    }

    @Override
    public void givePlayerCard(Card card) {
        playerCards.add(card);
    }

    @Override
    public Card getPlayedCard() {
        return cardOnTable;
    }

    public void playedCard(Card cardasd, List<Card> cardsOnTable, CardColor cardAdut){
        int cardValue = 0;
        Card topCard = getPlayerCards().get(0);
        boolean haveAdut = false;
        boolean haveColor = false;
        boolean haveHigherAdut = false;
        boolean haveHigher = false;
        boolean adutOn = false;

        for(Card card: getPlayerCards()){
            if(card.getCardColor() == cardAdut){
                haveAdut = true;
                break;
            }
            if(card.getCardColor()==cardsOnTable.get(0).getCardColor()){
                haveColor=true;
                break;
            }
        }

        for(Card card:cardsOnTable){
            if(card.getCardColor()==cardAdut){
                adutOn=true;
                break;
            }
        }

        for(Card card:getPlayerCards()){
            if(adutOn){
                if(haveAdut){
                    if(card.getCardColor()==cardAdut){
                        if(card.getValue()>cardOnTable.getValue()){
                            haveHigherAdut=true;
                        }
                    }
                }
            } else {
                if(haveColor) {
                    if(card.getValue()>cardOnTable.getValue()){
                        haveHigher=true;
                    }
                }
            }
        }

        for(Card card: getPlayerCards()){
            if(adutOn){
                if (haveAdut) {
                    if(haveHigherAdut){
                        if(card.getCardColor()==cardAdut){
                            if(card.getValue()>cardsOnTable.get(0).getValue()){
                                cardOnTable = card;
                            }
                        }
                    } else {
                        if(card.getCardColor()==cardAdut){
                            cardOnTable=card;
                        }
                    }
                } else {
                    if (haveColor) {
                        if (card.getCardColor() == cardsOnTable.get(0).getCardColor()) {
                            cardOnTable = card;
                        }
                    }
                }
            } else {
                if(haveColor){
                    if(haveHigher){
                        if(card.getCardColor()==cardOnTable.getCardColor()){
                            if(card.getValue()>cardOnTable.getValue()){
                                cardOnTable = card;
                            }
                        }
                    } else {
                        if(card.getCardColor()==cardOnTable.getCardColor()){
                            cardOnTable = card;
                        }
                    }
                } else {
                    cardOnTable = card;
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    public List<Card> getPlayerCards(){
        return playerCards;
    }

    public CardColor zovi(){
        return CardColor.BUNDEVA;
    }

    @Override
    public void setOnMove(boolean onMove) {
        this.onMove = onMove;
    }

    @Override
    public boolean onMove() {
        return onMove;
    }

}
