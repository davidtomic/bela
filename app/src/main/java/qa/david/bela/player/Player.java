package qa.david.bela.player;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.deck.CardColor;
import qa.david.bela.extra.classes.CardImageView;
import qa.david.bela.extra.classes.PlayerButton;

/**
 * Created by Pc on 26.7.2017..
 */

public interface Player{

    String getName();

    List<Card> getPlayerCards();

    void givePlayerCard(Card card);

    public Card getPlayedCard();

    public void playedCard(Card cardasd, List<Card> cardsOnTable, CardColor cardAdut);

    CardColor zovi();

    void setOnMove(boolean onMove);

    boolean onMove();
}