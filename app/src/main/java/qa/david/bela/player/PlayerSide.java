package qa.david.bela.player;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import qa.david.bela.extra.classes.CardImageView;

/**
 * Created by Pc on 18.8.2017..
 */

public class PlayerSide {

    private final Map<Player, CardImageView> players = new HashMap<Player, CardImageView>() {
        @Override
        public int size() {
            return 4;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsKey(Object o) {
            return false;
        }

        @Override
        public boolean containsValue(Object o) {
            return false;
        }

        @Override
        public CardImageView get(Object o) {
            return null;
        }

        @Override
        public CardImageView put(Player player, CardImageView cardImageView) {
            return null;
        }

        @Override
        public CardImageView remove(Object o) {
            return null;
        }

        @Override
        public void putAll(@NonNull Map<? extends Player, ? extends CardImageView> map) {

        }

        @Override
        public void clear() {

        }

        @NonNull
        @Override
        public Set<Player> keySet() {
            return null;
        }

        @NonNull
        @Override
        public Collection<CardImageView> values() {
            return null;
        }

        @NonNull
        @Override
        public Set<Entry<Player, CardImageView>> entrySet() {
            return null;
        }
    };

    public PlayerSide(List<Player> players, List<CardImageView> cardsOnTable){
        this.players.put(players.get(0), cardsOnTable.get(0));
        this.players.put(players.get(1), cardsOnTable.get(1));
        this.players.put(players.get(2), cardsOnTable.get(2));
        this.players.put(players.get(3), cardsOnTable.get(3));
    }

    public Map<Player, CardImageView> getPlayersSide(){
        return players;
    }
}
