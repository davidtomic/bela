package qa.david.bela;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.player.Player;
import qa.david.bela.player.PlayerHuman;

/**
 * Created by Pc on 26.7.2017..
 */

public class TablePlayers {

    private List<Player> players = new ArrayList<>(4);
    private Player firstPlayerOnMove;

    TablePlayers(Player player1, Player player2, Player player3, Player player4){
        this.players.add(player1);
        this.players.add(player2);
        this.players.add(player3);
        this.players.add(player4);
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Player getFirstPlayerOnMove() {
        return firstPlayerOnMove;
    }

    public List<Player> getPlayersByTurn(){
        List<Player> playersByTurn = new ArrayList<Player>(4);

        switch (firstPlayerOnMove){
            case players.get(0):
                playersByTurn = players;
                break;
            case players.get(1):
                playersByTurn.add(players.get(1));
                playersByTurn.add(players.get(2));
                playersByTurn.add(players.get(3));
                playersByTurn.add(players.get(0));
                break;
            case players.get(2):
                playersByTurn.add(players.get(2));
                playersByTurn.add(players.get(3));
                playersByTurn.add(players.get(0));
                playersByTurn.add(players.get(1));
                break;
            case players.get(3):
                playersByTurn.add(players.get(3));
                playersByTurn.add(players.get(0));
                playersByTurn.add(players.get(1));
                playersByTurn.add(players.get(2));
                break;
        }
        return playersByTurn;
    }

    public void setFirstPlayerOnMove(Player firstPlayerOnMove) {
        this.firstPlayerOnMove = firstPlayerOnMove;
    }

    public List<Player> getPlayers(){
        return players;
    }
}
