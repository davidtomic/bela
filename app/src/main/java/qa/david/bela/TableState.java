package qa.david.bela;

import android.widget.EditText;

import qa.david.bela.deck.CardColor;
import qa.david.bela.player.Player;

/**
 * Created by Pc on 17.8.2017..
 */

public class TableState {

    private Player playerOnMove;
    private CardColor adut;
    private boolean gameOn;
    private Player lastPlayerOnMove;
    private Player lastPlayerStart;

    public void setPlayerOnMove(Player playerOnMove){
        this.playerOnMove = playerOnMove;
    }

    public void setAdut(CardColor adut){
        this.adut = adut;
    }

    public void setGameOn(boolean gameOn) {
        this.gameOn = gameOn;
    }

    public void setLastPlayerOnMove(Player lastPlayerOnMove) {
        this.lastPlayerOnMove = lastPlayerOnMove;
    }

    public void setLastPlayerStart(Player lastPlayerStart) {
        this.lastPlayerStart = lastPlayerStart;
    }

    public Player getPlayerOnMove() {
        return playerOnMove;
    }

    public CardColor getAdut() {
        return adut;
    }

    public boolean isGameOn() {
        return gameOn;
    }

    public Player getLastPlayerOnMove() {
        return lastPlayerOnMove;
    }

    public Player getLastPlayerStart() {
        return lastPlayerStart;
    }

    public String toString(){
        return "Player on move = " + playerOnMove.getName() + "\n" +
                "Adut = " + adut + "\n" +
                "Game on = " + gameOn + "\n" +
                "Last player on move =" + lastPlayerOnMove.getName() + "\n" +
                "Last player start game =" + lastPlayerStart.getName();
    }
}
