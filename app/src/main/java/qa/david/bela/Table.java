package qa.david.bela;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.player.Player;
import qa.david.bela.team.Team;

/**
 * Created by Pc on 27.7.2017..
 */

public class Table {

    private TableAdut tableAdut;
    private TableDeck tableDeck;
    private TablePlayers tablePlayers;
    private TableScore tableScore;
    private List<Card> cardsOnTable = new ArrayList<>(4);
    private List<Team> tableTeamses = new ArrayList<>(2);
    private TableState tableState;

    public Table(TableDeck tableDeck, TablePlayers tablePlayers, TableScore tableScore) {
        this.tableDeck = tableDeck;
        this.tablePlayers = tablePlayers;
        this.tableScore = tableScore;
    }

    public List<Card> getCardsOnTable() {
        return cardsOnTable;
    }

    public void setCardsOnTable(Card card) {
        this.cardsOnTable.add(card);
    }

    public void setTableAdut(TableAdut tableAdut) {
        this.tableAdut = tableAdut;
    }

    public List<Team> getTableTeamses() {
        return tableTeamses;
    }



    public void setTableTeamses(List<Team> tableTeamses) {
        this.tableTeamses = tableTeamses;
    }

    public TableState getTableState() {
        return tableState;
    }

    public void setTableState(TableState tableState) {
        this.tableState = tableState;
    }

    public TableAdut getTableAdut() {
        return tableAdut;
    }

    public TableDeck getTableDeck() {
        return tableDeck;
    }

    public TablePlayers getTablePlayers() {
        return tablePlayers;
    }

    public TableScore getTableScore() {
        return tableScore;
    }

}
