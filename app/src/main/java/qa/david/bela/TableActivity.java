package qa.david.bela;

import android.content.pm.ActivityInfo;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import qa.david.bela.deck.Card;
import qa.david.bela.deck.CardColor;
import qa.david.bela.deck.CardType;
import qa.david.bela.deck.Deck;
import qa.david.bela.extra.classes.CardImageView;
import qa.david.bela.extra.classes.PlayerButton;
import qa.david.bela.menu.MenuFragment;
import qa.david.bela.player.Player;
import qa.david.bela.player.PlayerDetailsFragment;
import qa.david.bela.player.PlayerDroid;
import qa.david.bela.player.PlayerHuman;
import qa.david.bela.player.PlayerSide;
import qa.david.bela.team.Team;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class TableActivity extends AppCompatActivity {

    final FragmentManager fm = getSupportFragmentManager();

    CardColor currentAdutCardColor;

    final Deck deck = new Deck();
    final List<PlayerButton> playerButtons = new ArrayList<>(8);

    final TableDeck tableDeck = new TableDeck(deck.getShuffledDeck());
    final TableScore tableScore = new TableScore();
    final Player playerHuman = new PlayerHuman("You");
    final Player playertwo = new PlayerDroid("He");
    final Player playerBudala = new PlayerDroid("Him");
    final Player playerFour = new PlayerDroid("His");
    final TablePlayers tablePlayers = new TablePlayers(playerHuman, playertwo, playerBudala, playerFour);

    final Table table = new Table(tableDeck, tablePlayers, tableScore);

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        final ImageButton menu = (ImageButton) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMenu();
            }
        });

        final EditText state = (EditText)findViewById(R.id.state);
        state.setTextSize(5);
        state.setKeyListener(null);
        TableState tableState = new TableState();
        tableState.setPlayerOnMove(playerHuman);
        tableState.setLastPlayerOnMove(playerHuman);
        tableState.setLastPlayerStart(playerHuman);
        table.setTableState(tableState);
        state.setText(tableState.toString());
        updateTableState(tableState, state);

        final EditText chat = (EditText) findViewById(R.id.chat);
        chat.setTextSize(5);
        chat.setKeyListener(null);

        final ImageButton score = (ImageButton)findViewById(R.id.open_score_notes);

        final RelativeLayout scoreFragment = (RelativeLayout)findViewById(R.id.score);

        score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scoreFragment.getVisibility()==VISIBLE){
                    scoreFragment.setVisibility(GONE);
                } else {
                    scoreFragment.setVisibility(VISIBLE);
                }
            }
        });

        final PlayerButton cardOne = (PlayerButton) findViewById(R.id.card_one);
        final PlayerButton cardTwo = (PlayerButton) findViewById(R.id.card_two);
        final PlayerButton cardThree = (PlayerButton) findViewById(R.id.card_three);
        final PlayerButton cardFour = (PlayerButton) findViewById(R.id.card_four);
        final PlayerButton cardFive = (PlayerButton) findViewById(R.id.card_five);
        final PlayerButton cardSix = (PlayerButton) findViewById(R.id.card_six);
        final PlayerButton cardSeven = (PlayerButton) findViewById(R.id.card_seven);
        final PlayerButton cardEight = (PlayerButton) findViewById(R.id.card_eight);

        playerButtons.add(cardOne);
        playerButtons.add(cardTwo);
        playerButtons.add(cardThree);
        playerButtons.add(cardFour);
        playerButtons.add(cardFive);
        playerButtons.add(cardSix);
        playerButtons.add(cardSeven);
        playerButtons.add(cardEight);

        final CardImageView player1card = (CardImageView) findViewById(R.id.player_one_card);
        final CardImageView player2card = (CardImageView) findViewById(R.id.player_two_card);
        final CardImageView player3card = (CardImageView) findViewById(R.id.player_three_card);
        final CardImageView player4card = (CardImageView) findViewById(R.id.player_four_card);
        final List<CardImageView> igraneKarte = new ArrayList<>(4);
        igraneKarte.add(player1card);
        igraneKarte.add(player2card);
        igraneKarte.add(player3card);
        igraneKarte.add(player4card);

        final ImageButton player1 = (ImageButton)findViewById(R.id.player_one);
        final ImageButton player2 = (ImageButton)findViewById(R.id.player_two);
        final ImageButton player3 = (ImageButton)findViewById(R.id.player_three);
        final ImageButton player4 = (ImageButton)findViewById(R.id.player_four);
        final List<ImageButton> players = new ArrayList<>(4);
        players.add(player1);
        players.add(player2);
        players.add(player3);
        players.add(player4);

        for(final ImageButton player:players){
            player.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }

        final RelativeLayout zvanja = (RelativeLayout)findViewById(R.id.zvanje);
        final Button startGame = (Button)findViewById(R.id.continu);
        final ImageView currentAdut = (ImageView) findViewById(R.id.currentAdut);

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    podijeliKarte(currentAdut, startGame, playerButtons, igraneKarte, zvanja, chat, state);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void podijeliKarte(final ImageView currentAdut, final Button continueButton, final List<PlayerButton> playerButtons, final List<CardImageView> igraneKarte, final RelativeLayout zvanja, EditText chat, EditText state) throws InterruptedException {

        state.setText(table.getTableState().toString());

        Thread.sleep(600);

        continueButton.setVisibility(View.INVISIBLE);

        for(int i = 0 ; i < 4 ; i++){
            switch(i) {
                case 0:
                    for (int j = 0; j < 6; j++) {
                        table.getTablePlayers().getPlayers().get(0).givePlayerCard(table.getTableDeck().getDeck().get(j));
                    }
                    break;
                case 1:
                    for (int j = 0; j < 6; j++) {
                        table.getTablePlayers().getPlayers().get(1).givePlayerCard(table.getTableDeck().getDeck().get(j + 6));
                    }
                    break;
                case 2:
                    for (int j = 0; j < 6; j++) {
                        table.getTablePlayers().getPlayers().get(2).givePlayerCard(table.getTableDeck().getDeck().get(j + 12));
                    }
                    break;
                case 3:
                    for (int j = 0; j < 6; j++) {
                        table.getTablePlayers().getPlayers().get(3).givePlayerCard(table.getTableDeck().getDeck().get(j + 18));
                    }
                    break;
            }
        }

        chat.append("\n\nCards dealt");

        for(Player player:table.getTablePlayers().getPlayers()) {
            for(int j = 0; j <= 5; j++){
                player.givePlayerCard(table.getTableDeck().getDeck().get(j));
            }
        }

        for(int i = 0; i <=5;i++){
            playerButtons.get(i).setCard(table.getTablePlayers().getPlayers().get(0).getPlayerCards().get(i));
        }

        traziZvanja(currentAdut,continueButton,playerButtons, igraneKarte, zvanja, chat);

    }

    public void traziZvanja(final ImageView currentAdut, final Button continueButton, final List<PlayerButton> playerButtons, final List<CardImageView> igraneKarte, final RelativeLayout zvanja, final EditText chat) throws InterruptedException {



        Thread.sleep(600);

        final TableAdut tableAdut = new TableAdut();

        for(Player player:table.getTablePlayers().getPlayers()) {
            player.setOnMove(true);
            if (table.getTableState().getPlayerOnMove() == table.getTablePlayers().getPlayers().get(0)) {
                chat.append("\n\nPlayer one on move for calling adut");
                zvanja.setVisibility(View.VISIBLE);
                zvanja.findViewById(R.id.zoviBucu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chat.append("\n\nAdut is buca");
                        currentAdut.setImageResource(CardColor.BUNDEVA.getImage());
                        currentAdutCardColor = CardColor.BUNDEVA;
                        tableAdut.setCardColor(CardColor.BUNDEVA);
                        table.setTableAdut(tableAdut);
                        zvanja.setVisibility(GONE);
                        try {
                            if (currentAdutCardColor != null) {
                                giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                zvanja.findViewById(R.id.zoviHerca).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chat.append("\n\nAdut is herc");
                        currentAdut.setImageResource(CardColor.HERC.getImage());
                        currentAdutCardColor = CardColor.HERC;
                        tableAdut.setCardColor(CardColor.HERC);
                        table.setTableAdut(tableAdut);
                        zvanja.setVisibility(GONE);
                        try {
                            if (currentAdutCardColor != null) {
                                giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                zvanja.findViewById(R.id.zoviZelenu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chat.append("\n\nAdut is zelena");
                        currentAdut.setImageResource(CardColor.ZELENA.getImage());
                        currentAdutCardColor = CardColor.ZELENA;
                        tableAdut.setCardColor(CardColor.ZELENA);
                        table.setTableAdut(tableAdut);
                        zvanja.setVisibility(GONE);
                        try {
                            if (currentAdutCardColor != null) {
                                giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                zvanja.findViewById(R.id.zoviZira).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chat.append("\n\nAdut is zir");
                        currentAdut.setImageResource(CardColor.ZIR.getImage());
                        currentAdutCardColor = CardColor.ZIR;
                        tableAdut.setCardColor(CardColor.ZIR);
                        table.setTableAdut(tableAdut);
                        zvanja.setVisibility(GONE);
                        try {
                            if (currentAdutCardColor != null) {
                                giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                if (table.getTableState().getPlayerOnMove() == table.getTablePlayers().getPlayers().get(1)) {
                    chat.append("\n\nPlayer two on move for calling adut");
                    table.getTableState().setPlayerOnMove(table.getTablePlayers().getPlayers().get(2));
                    giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                } else if (table.getTableState().getPlayerOnMove() == table.getTablePlayers().getPlayers().get(2)) {
                    chat.append("\n\nPlayer three on move for calling adut");
                    table.getTableState().setPlayerOnMove(table.getTablePlayers().getPlayers().get(3));
                    giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                } else if (table.getTableState().getPlayerOnMove() == table.getTablePlayers().getPlayers().get(3)) {
                    chat.append("\n\nPlayer four on move for calling adut");
                    table.getTableState().setPlayerOnMove(table.getTablePlayers().getPlayers().get(0));
                    giveTalon(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);
                }
            }
        }

    }



    public void giveTalon(ImageView currentAdut, Button continueButton, List<PlayerButton> playerButtons, List<CardImageView> igraneKarte, RelativeLayout zvanja, EditText chat) throws InterruptedException {

        Thread.sleep(600);

        for(int i = 1; i <= 4; i++){
            switch(i){
                case 1:
                    for(int j = 1; j <= 2; j++){
                        table.getTablePlayers().getPlayers().get(i-1).givePlayerCard(table.getTableDeck().getDeck().get(j+23));
                    }
                    break;
                case 2:
                    for(int j = 1; j <= 2; j++){
                        table.getTablePlayers().getPlayers().get(i-1).givePlayerCard(table.getTableDeck().getDeck().get(j+25));
                    }
                    break;
                case 3:
                    for(int j = 1; j <= 2; j++){
                        table.getTablePlayers().getPlayers().get(i-1).givePlayerCard(table.getTableDeck().getDeck().get(j+27));
                    }
                    break;
                case 4:
                    for(int j = 1; j <= 2; j++){
                        table.getTablePlayers().getPlayers().get(i-1).givePlayerCard(table.getTableDeck().getDeck().get(j+29));
                    }
                    break;
            }
        }

        chat.append("\n\nTalon je podijeljen");

        staviAdut(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);

    }

    public void staviAdut(final ImageView currentAdut, final Button continueButton, final List<PlayerButton> playerButtons, final List<CardImageView> igraneKarte, RelativeLayout zvanja, EditText chat) throws InterruptedException {

        for (Player player : table.getTablePlayers().getPlayers()) {
            for (Card card : player.getPlayerCards()) {
                if (card.getCardColor() == table.getTableAdut().getCardColor()) {
                    card.setAdut(true);
                }
            }
        }

        for(int i =6;i<8;i++) {
            playerButtons.get(i).setCard(tablePlayers.getPlayers().get(0).getPlayerCards().get(i));
        }

        bacanje(currentAdut, continueButton, playerButtons, igraneKarte, zvanja, chat);

    }

    public void bacanje(final ImageView currentAdut, final Button continueButton, final List<PlayerButton> playerButtons, final List<CardImageView> igraneKarte, RelativeLayout zvanja, EditText chat) throws InterruptedException {

        Thread.sleep(600);

        if(table.getTablePlayers().getPlayers().get(0).onMove()) {
            chat.append("\n\nPlayer one on move for playing");
            for (final PlayerButton playerButton : playerButtons) {
                playerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkIfCardIsPlayable(playerButton.getCard(), table.getTablePlayers().getPlayers().get(0).getPlayerCards(), igraneKarte);
                        table.getTablePlayers().getPlayers().get(0).playedCard(playerButton.getCard(), table.getCardsOnTable(), currentAdutCardColor);
                        if (table.getTablePlayers().getPlayers().get(0).getPlayedCard() == null) {
                            igraneKarte.get(0).setCard(table.getTablePlayers().getPlayers().get(0).getPlayedCard());
                        } else {
                            igraneKarte.get(0).setCard(table.getTablePlayers().getPlayers().get(0).getPlayedCard());
                        }
                    }
                });
            }
        } else if (table.getTablePlayers().getPlayers().get(1).onMove()) {
            table.getTablePlayers().getPlayers().get(1).playedCard(null, table.getCardsOnTable(), currentAdutCardColor);
        }else if (table.getTablePlayers().getPlayers().get(2).onMove()) {
            table.getTablePlayers().getPlayers().get(2).playedCard(null, table.getCardsOnTable(), currentAdutCardColor);
        } else if (table.getTablePlayers().getPlayers().get(3).onMove()){
            table.getTablePlayers().getPlayers().get(3).playedCard(null, table.getCardsOnTable(), currentAdutCardColor);
        } else {

        }
    }

    public void getMenu() {
        MenuFragment menuFragment = new MenuFragment();
        menuFragment.show(fm, "");
    }

    public void getPlayerDetails(Player player) {
        PlayerDetailsFragment playerDetailsFragment = new PlayerDetailsFragment();
        playerDetailsFragment.show(fm, "");
    }

    public void checkIfCardIsPlayable(Card wantedCard, List<Card> playerCards, List<CardImageView> cardsOnTable){

        Card firstCard = cardsOnTable.get(0).getCard();
        Card highestCardOnTable = new Card(CardColor.ZELENA, CardType.SEDMICA);
        Card highestAdutOnTable = new Card(CardColor.ZELENA, CardType.SEDMICA);
        boolean adutIsOnTable = false;
        boolean colorIsOnTable = false;
        Card highestPlayerAdut = new Card(CardColor.ZELENA, CardType.SEDMICA);
        Card highestPlayerColor = new Card(CardColor.ZELENA, CardType.SEDMICA);
        boolean playerHaveAdut = false;
        boolean playerHaveColor= false;

        // Check if adut or cardColor is on table
        for(CardImageView card:cardsOnTable){
            if(!firstCard.isAdut()){
                if(card.getCard().isAdut()){
                    adutIsOnTable = true;
                }
            }
        }

        for(int i=0;i<playerCards.size();i++){
            if(playerCards.get(i).getCardColor()==firstCard.getCardColor()){
                playerHaveColor = true;
            } else if (playerCards.get(i).isAdut()){
                playerHaveAdut = true;
            }
        }

        // Check if there is any card on table that is same color as first one
        for(int i=0;i<4;i++){
            if(cardsOnTable.get(i).getCard().getCardColor()==cardsOnTable.get(0).getCard().getCardColor()){
                colorIsOnTable=true;
            }
        }

        //If card is bigger adut then any other adut on table
        for(CardImageView card:cardsOnTable){
            if(adutIsOnTable){
                if(card.getCard().isAdut()&&card.getCard().getValue()>highestAdutOnTable.getValue()){
                    highestAdutOnTable = card.getCard();
                }
            }
        }

        //
        if(!adutIsOnTable) {
            for (CardImageView card : cardsOnTable) {
                if (card.getCard().getCardColor() == firstCard.getCardColor() && card.getCard().getValue() > firstCard.getValue()) {
                    highestCardOnTable = card.getCard();
                }
            }
        }
    }

    private void updateScore(){
        for(Team team :table.getTableTeamses()){

        }
    }

    public void updateTableState(TableState tableState, EditText state){
        state.setText(tableState.toString());
    }

    public void setPlayerOnMove(Player player){

        for(Player player1:table.getTablePlayers().getPlayers()){
            if(player==player1){
                player.setOnMove(true);
            } else {
                player.setOnMove(false);
            }
        }
        table.getTableState().setPlayerOnMove(player);
    }

    public void baciCetriKarte(EditText chat, List<CardImageView> igraneKarte){
        for(Player player:table.getTablePlayers().getPlayers()){
            switch (player){
                case playerHuman:
                    table.getTableState().setPlayerOnMove(playerHuman);
                    humanBaciKartu(chat, igraneKarte);
                    break;
                case playerBudala:
                    table.getTableState().setPlayerOnMove(playerBudala);
                    baciKartu();
                    break;
                case playertwo:
                    table.getTableState().setPlayerOnMove(playertwo);
                    baciKartu();
                    break;
                case playerFour:
                    table.getTableState().setPlayerOnMove(playerFour);
                    baciKartu();
                    break;
            }
        }
    }

    public void humanBaciKartu(final EditText chat, final List<CardImageView> igraneKarte){
        final boolean moraPostivat = false;

        for(CardImageView cardImageView:igraneKarte){
            if(cardImageView!=null){
                moraPostivat = true;
            } else {
                moraPostivat = false;
            }
        }

        for (final PlayerButton playerButton : playerButtons) {
            playerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(moraPostivat) {
                        checkIfCardIsPlayable(playerButton.getCard(), table.getTablePlayers().getPlayers().get(0).getPlayerCards(), igraneKarte);
                        table.getTablePlayers().getPlayers().get(0).playedCard(playerButton.getCard(), table.getCardsOnTable(), currentAdutCardColor);
                        if (table.getTablePlayers().getPlayers().get(0).getPlayedCard() == null) {
                            igraneKarte.get(0).setCard(table.getTablePlayers().getPlayers().get(0).getPlayedCard());
                        } else {
                            igraneKarte.get(0).setCard(table.getTablePlayers().getPlayers().get(0).getPlayedCard());
                        }
                    }
                }
            });
        }
    }

    public void baciKartu(){

    }
}