package qa.david.bela;

import android.widget.ImageView;

import qa.david.bela.deck.CardColor;

/**
 * Created by Pc on 26.7.2017..
 */

public class TableAdut {

    private CardColor cardColor;

    public CardColor getCardColor() {
        return cardColor;
    }

    public void setCardColor(CardColor cardColor) {
        this.cardColor = cardColor;
    }
}
