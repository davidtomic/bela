package qa.david.bela;

import java.util.ArrayList;

import qa.david.bela.extra.classes.CardImageView;

/**
 * Created by Pc on 27.7.2017..
 */

public class TableCards {

    private ArrayList<CardImageView> cardImageViews;

    public void setCardImageViews(ArrayList<CardImageView> cardImageViews) {
        this.cardImageViews = cardImageViews;
    }

    public ArrayList<CardImageView> getCardImageViews() {
        return cardImageViews;
    }

}
