package qa.david.bela.menu;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import qa.david.bela.R;

/**
 * Created by Pc on 27.7.2017..
 */

public class MenuFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.menu_fragment, container,
                false);

        Button startGame = (Button)rootView.findViewById(R.id.start_game);
        Button exitGame = (Button) rootView.findViewById(R.id.exit_game);
        EditText name = (EditText) rootView.findViewById(R.id.menu_name);

        startGame.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        exitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        return rootView;
    }

}
